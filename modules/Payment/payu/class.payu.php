<?php


class payu extends TokenPaymentModule
{

    use \Components\Traits\LoggerTrait;

    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.2019-05-31';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'payU';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'payU Online Checkout module';


    protected $supportedCurrencies = ['BGN','CHF','CZK','DKK','EUR','GBP','HRK','HUF','NOK', 'PLN', 'RON', 'RUB', 'SEK', 'UAH', 'USD'];

    protected $configuration = [

        'POS ID' => [
            'value' => '',
            'type' => 'input'
        ],

        'Second key' => [
            'value' => '',
            'type' => 'input'
        ],

        'client_id' => [
            'value' => '',
            'type' => 'input'
        ],

        'client_secret' => [
            'value' => '',
            'type' => 'input'
        ],


        'Test mode' => [
            'value' => '',
            'description' => 'Tick if your Rave environment is using Sandbox mode',
            'type' => 'check'
        ],


    ];

    private $token = '';

    public function __construct()
    {


        parent::__construct();

    }


    /**
     * HostBill will run this function first time module is activated.
     * Use it to create some database tables
     */
    public function install()
    {

    }

    /**
     * HostBill will run this function when admin choose to "Uninstall" module.
     * Use it to truncate/remove tables.
     */
    public function uninstall()
    {

    }


    private function setToken(){
              $params = $this->connectionCurl(1, '' );
              $token_type = ucfirst($params['token_type']);
              $access_token = $params['access_token'];
              $token = $token_type.' '.$access_token;
              $this->token = $token;


    }

    public function getToken() {
        return $this->token;
    }


    public function connectionCurl($type, $data, $id_order = 0) {

        switch($type){
            case 1:
                if($this->configuration['Test mode']['value'])
                    $url= 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize';
                else
                    $url = 'https://secure.payu.com/pl/standard/user/oauth/authorize';
                $fields =http_build_query(['grant_type'=>'client_credentials', 'client_id'  => $this->configuration['client_id']['value'], 'client_secret' => $this->configuration['client_secret']['value']]);
                $headers = ["Content-Type: application/x-www-form-urlencoded"];
                break;
            case 2:
                if($this->configuration['Test mode']['value'])
                    $url= 'https://secure.snd.payu.com/api/v2_1/orders/';
                else
                    $url = 'https://secure.payu.com/api/v2_1/orders/';
                $fields = json_encode($data);
                $headers = ['Content-Type: application/json', 'Authorization: '.$this->getToken()];
                break;
            case 3:
                if($this->configuration['Test mode']['value'])
                    $url = 'https://secure.snd.payu.com/api/v2_1/orders/'.$id_order.'/refund';
                else
                    $url = 'https://secure.payu.com/api/v2_1/orders/'.$id_order.'/refund';
                $fields = json_encode($data);
                $headers = ['Content-Type: application/json', 'Authorization: '.$this->getToken()];
                break;


        }


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        $error = curl_error($ch);

        curl_close($ch);

        if($error){
            $this->logActivity([
                'output' => 'Connection error ',
                'result' => PaymentModule::PAYMENT_FAILURE
            ]);

            return false;
        }

        return json_decode($response, 1);

    }


    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details)
     * @return string
     */

    public function drawForm() {
        $this->setToken();




        $ccard = HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id);

        $orderData = $this->prepareData();
        $response = $this->connectionCurl(2, $orderData);
        if(!isset($response['status']['statusCode']) OR $response['status']['statusCode']!='SUCCESS'){

            $this->logActivity([
                'output' => ['error' => 'Cannot create payU redirect url: '.$response['status']['statusDesc'].', code: '.$response['status']['statusCode']],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");

            return false;
        }


        $url = $response['redirectUri'];

        $form ='<form action="'.$url.'" method="post" name="payform">';
        $form .= '<input type="submit" value="'.$this->paynow_text().'" />';
        $form .= '</form>';

        return $form;


    }

    public function prepareData() {
        $amount = $this->amount * 100;
        $amount = (string)$amount;

        $data = [];
        $data['notifyUrl'] = $this->callback_url;
        $data['continueUrl'] = Utilities::url('?cmd=clientarea&action=invoice&id='.$this->invoice_id);
        $data['customerIp'] = $this->getUserIpAddr();
        $data['merchantPosId'] = $this->configuration['client_id']['value'] ? $this->configuration['client_id']['value'] : $this->configuration['POS ID']['value'];
        $data['description'] = 'Invoice '.$this->invoice_id;
        $data['currencyCode'] =  'PLN';//$this->currency_code;
        $data['totalAmount'] =  $amount;
        //$data['extOrderId'] = $this->invoice_id;
        $data['buyer'] = [
            'email' => $this->client['email'],
            'phone' => $this->client['phonenumber'],
            'firstName' => $this->client['firstname'],
            'lastname' => $this->client['lastname'],
            'language' => 'pl',
        ];

        if($this->configuration['Test mode']['value']){
            $data['settings'] = ['invoiceDisabled' => "true"];
        }

        $data['products'][] = [
                'name' => 'Invoice '.$this->invoice_id,
                'unitPrice' => $amount,
                'quantity' => 1
            ];


        return $data;
    }

    public function capture_token($params)
    {



    }
    
        /**
     * Handle payment refund
     *
     * @param $_transaction
     */
    public function refund($_transaction, $amount) {

        $this->setToken();
        $data = [];
        $data['refund'] = [
            'description'   => 'Refund',
            'amount'    => (string)($amount * 100)
        ];
        $data['orderId'] = $_transaction;


        $result = $this->connectionCurl(3, $data, $_transaction);

        if(isset($result['status']['code'])){
            $this->logActivity([
                'output' => ['error' => $result['status']['codeLiteral'].', '.$result['status']['statusDesc'] ],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Refund failed please contact administrator");
            return false;
        }
        elseif(isset($result['status']['statusCode']) AND $result['status']['statusCode']=='SUCCESS'){
            $result = $this->addRefund(array(
                'target_transaction_number' => $_transaction,
                'transaction_id' => $result['orderId'],
                'amount' => $amount
            ));
            $this->logActivity(array(
                'output' =>  $_transaction,
                'result' =>  self::PAYMENT_SUCCESS
            ));
            return $result;
        }

        $this->logActivity(array(
            'output' => 'Faild to create refund: '.$_transaction,
            'result' => PaymentModule::PAYMENT_FAILURE
        ));

        return false;

    }
    


    public function callback(){

        $body = @file_get_contents('php://input');
        $data = json_decode($body, true);
        $headers = getallheaders();

        foreach($headers AS $key=>$header){



                if($key=='Openpayu-Signature'){

                   $headers_array = explode(';', $header);
                   $signature = explode('=', $headers_array[1]);
                   $signature = $signature[1];

                }
        }

        if($signature!=md5($body.''.$this->configuration['Second key']['value'])){
            $this->logActivity([
                'output' => ['error' => 'Wrong signature '.$signature],
                'result' => self::PAYMENT_FAILURE
            ]);
        }
        elseif($data['order']['status']!='COMPLETED') {

            return false;
        }
        else{
            $invoice_id = str_replace('Invoice ', '', $data['order']['description']);
            $order_id = $data['order']['orderId'];
            $amount = $data['order']['totalAmount']/100;
            $invoice = Invoice::createInvoice($invoice_id);
            $result = self::PAYMENT_SUCCESS;


            if (!$this->_transactionExists($order_id)) {
                $this->addTransaction([
                    'invoice_id' => $invoice_id,
                    'client_id' => $invoice->getClientId(),
                    'in' => $amount,
                    'transaction_id' => $order_id,
                    'description' => 'payU Payment ' . $invoice_id.' Total paid: '.$amount,
                    'fee' => 0
                ]);
            }
            $log['transaction'] = $order_id;
            $this->logActivity([
                'output' => ['transaction' => $order_id],
                'result' => $result
            ]);
            return $order_id;
        }

        $this->logActivity([
            'output' => ['error2' => $body],
            'result' => self::PAYMENT_FAILURE
        ]);



    }

    public function getCartHTML() {
        return "";
    }

    public function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}





